const path = require("path");
const fs = require("fs");
const { isStartWith, isolate, sum, readableBytes } = require("../helpers");
const findSize = require("./findSize");
var readline = require("readline");

const isStartWithDot = isStartWith(".");

const errHandler = err => {
  console.error(err.message || err);
  process.exit(1);
};

const arr = [];
let sizes = 0;

const logging = (from = 0) => msg => {
  readline.cursorTo(process.stdout, 0, from);
  readline.clearScreenDown(process.stdout);
  process.stdout.write(msg + "\n");
  process.stdout.write("size : " + readableBytes(sizes) + "\n");
};

function crawling(curDir, options) {
  const { right: files, left: err } = isolate(fs.readdirSync)(curDir);

  if (err) return errHandler(err);

  files.forEach(file => {
    const fullPath = path.join(curDir, file);
    logging(0)(fullPath);

    if (options.ignoreHiddenFolder && isStartWithDot(file)) return;

    const { right, left } = isolate(fs.statSync)(fullPath);

    if (left) return;

    if (right.isFile()) {
      if (file === options.target) return arr.push(fullPath);
      return;
    }

    if (right.isDirectory()) {
      if (file === options.target) {
        const data = findSize(fullPath);
        sizes = sizes + sum(data.map(i => i.size));
        return arr.push(fullPath);
      }
      return crawling(fullPath, options);
    }
  });

  return { arr, sizes };
}

module.exports = crawling;
