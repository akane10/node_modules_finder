const inquirer = require("inquirer");
const rimraf = require("rimraf");
const R = require("ramda");
const path = require("path");

const getParent = str => {
  const x = str.split(path.sep);
  return ([child, parent] = R.reverse(x));
};

async function kill(arr) {
  const questions = [
    {
      name: "node_module_path",
      type: "checkbox",
      message: "Choose node modules you want to delete",
      choices: arr
    }
  ];

  const { node_module_path } = await inquirer.prompt(questions);

  node_module_path.forEach(i => {
    rimraf(i, function() {
      const [child, parent] = getParent(i);
      console.log(`"${child}" in "${parent}" has been deleted`);
    });
  });
}

module.exports = kill;
