const path = require("path");
const fs = require("fs");
const { isolate } = require("../helpers");

const processExit = err => {
  console.error(err.message || err);
  process.exit(1);
};

const arr = [];

function findSize(curDir) {
  const { right: files, left: err } = isolate(fs.readdirSync)(curDir);

  if (err) return processExit(err);

  files.forEach(file => {
    const fullPath = path.join(curDir, file);

    const { right, left } = isolate(fs.statSync)(fullPath);

    if (left) {
      if (left.code === "ENOENT") return;
      return processExit(left);
    }

    const obj = {
      path: fullPath,
      size: right.size
    };

    if (right.isFile()) return arr.push(obj);

    if (right.isDirectory()) {
      arr.push(obj);
      return findSize(fullPath);
    }
  });

  return arr;
}

module.exports = findSize;
