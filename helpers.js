// const fs = require("fs");

/**
 * Converts a long string of bytes into a readable format e.g KB, MB, GB, TB, YB
 *
 * @param {Int} num The number of bytes.
 */
function readableBytes(num) {
  const neg = num < 0;
  const units = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  if (neg) {
    num = -num;
  }

  if (num < 1) return (neg ? "-" : "") + num + " B";

  const exponent = Math.min(
    Math.floor(Math.log(num) / Math.log(1000)),
    units.length - 1
  );

  num = Number((num / Math.pow(1000, exponent)).toFixed(2));
  const unit = units[exponent];

  return (neg ? "-" : "") + num + " " + unit;
}

// composeEither :: Either a b -> (b -> Either a c) -> Either a c
const composeEither = (...fns) => x => {
  return fns.reduceRight((e, f) => {
    // console.log("e", e);
    const { left, right } = e;
    if (left) return { left };
    return f(right);
  }, x);
};

// either :: (a -> c) -> (b -> c) -> Either a b -> c
const either = fnl => fnr => ({ right, left }) => {
  if (right) return fnr(right);
  return fnl(left);
};

// isStartWith :: Char -> String -> Bool
const isStartWith = char => str => {
  const [h] = str.split("");
  return h === char;
};

// isolate :: (b -> c) -> b -> Either a c
const isolate = fn => params => {
  try {
    const a = fn(params);
    return { right: a };
  } catch (e) {
    return { left: e };
  }
};

// sum :: [Int] -> Int
const sum = arr => arr.reduce((acc, cv) => acc + cv, 0);

function msToTime(duration) {
  var milliseconds = parseInt((duration % 1000) / 100),
    seconds = Math.floor((duration / 1000) % 60),
    minutes = Math.floor((duration / (1000 * 60)) % 60),
    hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
}

const stopwatch = {
  start: () => new Date(),
  stop: time => msToTime(new Date() - time)
};

module.exports = {
  composeEither,
  either,
  isStartWith,
  isolate,
  readableBytes,
  sum,
  stopwatch
};
