const crawling = require("./src/crawling");
const kill = require("./src/kill");
const { readableBytes, stopwatch } = require("./helpers");
const inquirer = require("inquirer");

const { HOME } = process.env;

const askToContinue = async () => {
  const questions = [
    {
      name: "askToContinue",
      type: "confirm",
      message: "Do you want continue to delete?",
      default: false
    }
  ];

  return await inquirer.prompt(questions);
};

(async function main() {
  const options = {
    ignoreHiddenFolder: true,
    folder: true,
    target: "node_modules"
  };

  // console.log("walking...");
  // console.log(`root "${HOME}"`);
  const start = stopwatch.start();
  const { arr: data, sizes } = crawling(HOME, options);
  console.log(`found ${data.length} ${options.target}`);
  console.log(`total size "${readableBytes(sizes)}" (${sizes} bytes)`);
  console.log(stopwatch.stop(start));

  const { askToContinue: isContinue } = await askToContinue();
  if (isContinue) return kill(data);
})();
